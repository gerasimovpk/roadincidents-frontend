package ru.letalo.roadincidentsmobile;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import ru.letalo.roadincidentsmobile.adapters.CustomArrayAdapter;
import ru.letalo.roadincidentsmobile.entities.Drone;
import ru.letalo.roadincidentsmobile.entities.Incident;
import ru.letalo.roadincidentsmobile.entities.IncidentReport;

public class Report extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        setUpMapIfNeeded(getIntent().getExtras().getInt("id"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded(getIntent().getExtras().getInt("id"));
    }

    private void setUpMapIfNeeded(int incidentId) {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap(incidentId);
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap(int incidentId) {
        IncidentReport rep = getIncidentReport(incidentId);
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build());
        mMap.addMarker(new MarkerOptions().position(new LatLng(rep.getDronePosLat(), rep.getDronePosLon())).title(getResources().getString(R.string.dronePosition)));
        mMap.addMarker(new MarkerOptions().position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())).title(getResources().getString(R.string.yourPosition)));
    }

    private IncidentReport getIncidentReport(int id) {
        // TODO: get data from server
        // stub

        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet("https://road-incidents.herokuapp.com//report?id=1");
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        Drone drone = null;
        Double lat = new Double(0);
        Double lon = new Double(0);
        Incident incident = null;
        try {
            HttpResponse response = client.execute(get);

            HttpEntity r_entity = response.getEntity();
            String xmlString = EntityUtils.toString(r_entity);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inStream = new InputSource();
            inStream.setCharacterStream(new StringReader(xmlString));
            Document doc = db.parse(inStream);

            NodeList nl = doc.getElementsByTagName("report");

            for(int i = 0; i < nl.getLength(); i++) {

                if (nl.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    NodeList children = nl.item(i).getChildNodes();

                    for(int j = 0; j < children.getLength(); j++) {
                        if (children.item(j).getNodeName().equals("drone")) {
                            drone = new Drone(i,
                                    children.item(j).getAttributes().getNamedItem("name").getNodeValue(),
                                    children.item(j).getAttributes().getNamedItem("model").getNodeValue());
                        } else if (children.item(j).getNodeName().equals("lat")) {
                            lat = Double.parseDouble(children.item(j).getTextContent()) ;
                        } else if (children.item(j).getNodeName().equals("lon")) {
                            lon = Double.parseDouble(children.item(j).getTextContent());
                        } else if (children.item(j).getNodeName().equals("incident")) {
                            incident = new Incident(i,
                                    children.item(j).getAttributes().getNamedItem("address").getNodeValue(),
                                    Double.parseDouble(children.item(j).getAttributes().getNamedItem("lat").getNodeValue()),
                                    Double.parseDouble(children.item(j).getAttributes().getNamedItem("lon").getNodeValue()),
                                    Integer.parseInt(children.item(j).getAttributes().getNamedItem("status").getNodeValue()));
                        }
                    }
                }
            }


        } catch (Exception e) {
            Log.e("RESPONSE", "is " + e.getMessage());
        }

        return new IncidentReport(drone,incident,lat,lon);
    }
}
