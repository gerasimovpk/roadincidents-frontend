package ru.letalo.roadincidentsmobile.entities;

/**
 * Created by gerasimov.pk on 21.12.2014.
 */
public class IncidentReport {
    private Drone drone;
    private Incident incident;
    private Double dronePosLat;
    private Double dronePosLon;
    private String photoUrl;

    public IncidentReport(Drone drone, Incident incident, Double dronePosLat, Double dronePosLon) {
        this.drone = drone;
        this.incident = incident;
        this.dronePosLat = dronePosLat;
        this.dronePosLon = dronePosLon;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Double getDronePosLat() {
        return dronePosLat;
    }

    public void setDronePosLat(Double dronePosLat) {
        this.dronePosLat = dronePosLat;
    }

    public Double getDronePosLon() {
        return dronePosLon;
    }

    public void setDronePosLon(Double dronePosLon) {
        this.dronePosLon = dronePosLon;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
