package ru.letalo.roadincidentsmobile.entities;

/**
 * Created by gerasimov.pk on 21.12.2014.
 */
public class Incident {
    private int id;
    private String address;
    private Double lat;
    private Double lon;
    private int status;

    public Incident(int id, String address, Double lat, Double lon, int status) {
        this.id = id;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusString() {
        return this.status == 0 ? "Заявка принята"
                : this.status == 1 ? "Заявка выполняется"
                    : this.status == 2 ? "Заявка выполнена" : "Неизвестно";
    }

    public void setStatus(int status) {
        if (!(status==1||status==2||status==3)) {
            throw new IllegalArgumentException("Accept only 1,2,3");
        }
        this.status = status;
    }
}
