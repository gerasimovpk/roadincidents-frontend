package ru.letalo.roadincidentsmobile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import ru.letalo.roadincidentsmobile.adapters.CustomArrayAdapter;
import ru.letalo.roadincidentsmobile.entities.Incident;


public class RequestList extends ActionBarActivity {

    private ArrayList al = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_list);
        initList();
    }

    private void initList() {
        final RequestList _this = this;
        ListView list = (ListView) findViewById(R.id.listView);
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet("https://road-incidents.herokuapp.com/list");
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        try {
            HttpResponse response = client.execute(get);

            HttpEntity r_entity = response.getEntity();
            String xmlString = EntityUtils.toString(r_entity);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inStream = new InputSource();
            inStream.setCharacterStream(new StringReader(xmlString));
            Document doc = db.parse(inStream);

            NodeList nl = doc.getElementsByTagName("request");
            for(int i = 0; i < nl.getLength(); i++) {

                if (nl.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    NodeList children = nl.item(i).getChildNodes();
                    String address = "";
                    Double lat = new Double(0);
                    Double lon = new Double(0);
                    Integer status = new Integer(0);
                    for(int j = 0; j < children.getLength(); j++) {
                        if (children.item(j).getNodeName().equals("address")) {
                            address = children.item(j).getTextContent();
                        } else if (children.item(j).getNodeName().equals("lat")) {
                            lat = Double.parseDouble(children.item(j).getTextContent()) ;
                        } else if (children.item(j).getNodeName().equals("lon")) {
                            lon = Double.parseDouble(children.item(j).getTextContent());
                        } else if (children.item(j).getNodeName().equals("status")) {
                            status = Integer.parseInt(children.item(j).getTextContent());
                        }
                    }
                    al.add(new Incident(i, address, lat, lon,status));
                }
            }

            CustomArrayAdapter adapter = new CustomArrayAdapter(this, al);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                        long arg3) {
                    Incident i = (Incident) al.get(position);
                    Intent intent = new Intent(_this, Report.class);
                    intent.putExtra("id", i.getId());
                    startActivity(intent);
                }
            });

        } catch (Exception e) {
            Log.e("RESPONSE", "is " + e.getMessage());
        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_request_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createRequestHandler (View view) {
        Intent intent = new Intent(this, CreateRequest.class);
        startActivity(intent);
    }
}
