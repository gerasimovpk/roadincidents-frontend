package ru.letalo.roadincidentsmobile.adapters;

/**
 * Created by gerasimov.pk on 21.12.2014.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.letalo.roadincidentsmobile.R;
import ru.letalo.roadincidentsmobile.entities.Incident;

public class CustomArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final ArrayList values;

    public CustomArrayAdapter(Context context, ArrayList values) {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        TextView address = (TextView) rowView.findViewById(R.id.address);
        TextView status = (TextView) rowView.findViewById(R.id.status);
        address.setText(((Incident)values.get(position)).getAddress());
        status.setText(((Incident)values.get(position)).getStatusString());
        return rowView;
    }
}
